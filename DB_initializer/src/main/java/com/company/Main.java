package com.company;

import java.io.Console;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class Main {

    static Connection connection = null;

    public static void main(String[] args) throws ClassNotFoundException, SQLException
    {
        try {
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection("jdbc:postgresql://pg:5432/studs", "s312478", "kvj383");
        }catch (java.sql.SQLException e)
        {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }

        System.out.println("Если хотите что-то инициализировать напишите \"init\", если удалить -- \"del\"");

        Console console = System.console();
        if(console.readLine().equals("delete"))
            delete();
        if (console.readLine().equals("init"))
            init();

    }

    private static void init() throws SQLException
    {
        Statement statement = connection.createStatement();

        Console console = System.console();

        System.out.println("spacemarines\nusers\nsequence?");

        String component = console.readLine();

        if(component.equals("spacemarines"))
            statement.execute("create table spacemarines(id bigint primary key, name varchar, x real, y bigint, creation_date date, health double precision, category varchar, weapon varchar, melee_weapon varchar, chapter_name varchar, chapter_legion varchar, login varchar);");

        if(component.equals("users"))
            statement.execute("create table users(login varchar primary key, password varchar);");

        if(component.equals("sequence")) {
            statement.execute("create sequence id_generator increment by 1 start 1;");
            statement.execute("select nextval('id_generator');");
        }
    }

    private static void delete() throws SQLException
    {
        Statement statement = connection.createStatement();

        Console console = System.console();

        System.out.println("spacemarines\nusers\nsequence?");

        String component = console.readLine();

        if(component.equals("spacemarines"))
            statement.execute("drop table spacemarines;");

        if(component.equals("sequence"))
            statement.execute("drop sequence id_generator;");

        if(component.equals("users"))
            statement.execute("drop table users;");

    }
}
