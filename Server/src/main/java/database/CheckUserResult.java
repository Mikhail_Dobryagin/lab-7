package database;

public enum CheckUserResult
{
    WRONG_USER,
    WRONG_PASSWORD,
    RIGHT
}
