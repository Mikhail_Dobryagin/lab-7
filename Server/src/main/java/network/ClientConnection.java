package network;

import util.ErrorReturn;
import util.Pair;

import java.io.*;
import java.net.Socket;

public class ClientConnection
{
    private Socket socket;

    public Socket getSocket()
    {
        return socket;
    }

    public void setSocket(Socket socket)
    {
        this.socket = socket;
    }

    public ClientConnection(Socket socket)
    {
        this.socket = socket;
    }

    public Pair<ErrorReturn, PacketCtoS> input()
    {
        try {
            InputStream inputStreamFromSocket = socket.getInputStream();
            ObjectInputStream inputStream = new ObjectInputStream(inputStreamFromSocket);
            Object in = inputStream.readObject();

            PacketCtoS inputPacket = (PacketCtoS) in;

            return inputPacket == null ? new Pair<>(new ErrorReturn(2, "Произошла ошибка"), null) :
                    new Pair<>(ErrorReturn.OK(), inputPacket);
        } catch (ClassNotFoundException | ClassCastException | IOException e) {
            e.printStackTrace();
            return new Pair<>(new ErrorReturn(2, e.getMessage()), null);
        }


    }

    public ErrorReturn output(PacketStoC args)
    {
        try {
            ByteArrayOutputStream internalByteArrayOutputStream = new ByteArrayOutputStream();
            ObjectOutputStream internalObjectOS = new ObjectOutputStream(internalByteArrayOutputStream);
            internalObjectOS.writeObject(args);


            ObjectOutputStream outputStream = new ObjectOutputStream(socket.getOutputStream());
            outputStream.writeInt(4 + internalByteArrayOutputStream.size() + 2); //+2? (May be, header)
            outputStream.writeObject(args);

            return ErrorReturn.OK();
        } catch (ClassCastException | IOException e) {
            e.printStackTrace();
            return new ErrorReturn(2, "Ошибка при попытке ответа клиенту");
        }
    }
}
