package network;

import util.ErrorReturn;

import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

public class OutputThread extends Thread
{
    ClientConnection clientConnection;
    PacketStoC outputPacket;
    Logger logger;
    LogRecord messageForLogger;

    public OutputThread(ClientConnection clientConnection, PacketStoC outputPacket, Logger logger, LogRecord messageForLogger)
    {
        this.clientConnection = clientConnection;
        this.outputPacket = outputPacket;
        this.logger = logger;
        this.messageForLogger = messageForLogger;
    }

    @Override
    public void run()
    {
        logger.log(messageForLogger);

        ErrorReturn errorReturn = clientConnection.output(outputPacket);

        if (errorReturn.getCode() != 0)
            logger.log(Level.WARNING, errorReturn.getStatus());
    }
}
