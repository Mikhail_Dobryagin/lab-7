package network;

import com.company.Invoker;
import database.User;
import util.ErrorReturn;
import util.Pair;

import java.util.concurrent.ExecutorService;
import java.util.function.Predicate;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

public class InputThread extends Thread
{
    ClientConnection clientConnection;
    Logger logger;
    Invoker invoker;
    ExecutorService threadPool;

    Predicate<User> checkUser;

    public InputThread(ClientConnection clientConnection, Logger logger, Invoker invoker, ExecutorService threadPool, Predicate<User> checkUser)
    {
        super();
        this.clientConnection = clientConnection;
        this.logger = logger;
        this.invoker = invoker;
        this.threadPool = threadPool;
        this.checkUser = checkUser;
    }

    @Override
    public void run()
    {
        logger.log(Level.INFO, "Получение данных от клиента...");

        Pair<ErrorReturn, PacketCtoS> errorWithReturn = clientConnection.input(); //commandName + login + password + args(may be not)

        PacketCtoS inputPacket = errorWithReturn.second;

        PacketStoC outputPacket = new PacketStoC();

        User user = inputPacket.getUser();

        if (errorWithReturn.first.getCode() != 0)          //Если ошибка
        {
            outputPacket.setErrorReturn(errorWithReturn.first);
            logger.log(Level.WARNING, errorWithReturn.first.getStatus());
            (new OutputThread(clientConnection, outputPacket, logger, new LogRecord(Level.INFO, "Отправка данных об ошибке клиенту: \"" + user.getLogin() + "\""))).start();
            return;
        }

        if (!(inputPacket.getCommandName().equals("check_user") || inputPacket.getCommandName().equals("register")) && !checkUser.test(user)) {
            outputPacket.setErrorReturn(new ErrorReturn(1, "Не удалось пройти проверку подлинности"));
            (new OutputThread(clientConnection, outputPacket, logger, new LogRecord(Level.INFO, "Отправка данных об ошибке клиенту: \"" + user.getLogin() + "\""))).start();
            return;
        }

        Runnable processing = () -> {
            logger.log(Level.INFO, "Исполнение команды...");
            PacketStoC outputPacket1 = invoker.execute(inputPacket);

            (new OutputThread(clientConnection, outputPacket1, logger, new LogRecord(Level.INFO, "Отправка данных клиенту \"" + user.getLogin() + "\""))).start();
        };

        threadPool.execute(processing);
    }
}
