package network;


import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;


public class NetworkConnection
{
    private int PORT = 7531;
    private ServerSocket serverSocket;

    /////////////////////////////////////////////////////////////////////////////////

    public int getPORT()
    {
        return PORT;
    }

    public void setPORT(int PORT)
    {
        this.PORT = PORT;
    }

    public ServerSocket getServerSocket()
    {
        return serverSocket;
    }

    public void setServerSocket(ServerSocket serverSocket)
    {
        this.serverSocket = serverSocket;
    }


    /////////////////////////////////////////////////////////////////////////////////
    public NetworkConnection(int PORT) throws IOException
    {
        this.PORT = PORT;
        serverSocket = getServer(this.PORT);
    }

    public NetworkConnection() throws IOException
    {
        serverSocket = getServer(this.PORT);
    }

    private static ServerSocket getServer(int PORT) throws IOException
    {
        return new ServerSocket(PORT);
    }

    public ClientConnection getNewClientConnection()
    {
        try {
            serverSocket.setSoTimeout(100);
            Socket socket = serverSocket.accept();
            return new ClientConnection(socket);
        } catch (IOException e) {
            return null;
        }
    }
}
