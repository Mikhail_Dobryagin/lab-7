package spacemarine;

import java.io.Serializable;

/**
 * Дополнительное оружие десантника
 */
public enum MeleeWeapon implements Serializable
{
    POWER_SWORD,
    MANREAPER,
    LIGHTING_CLAW,
    POWER_BLADE,
    POWER_FIST;

    public static final long serialVersionUID = 4L;
}
