package com.company;

import commands.*;
import database.CheckUserResult;
import database.StudsDbConnection;
import network.ClientConnection;
import network.InputThread;
import network.NetworkConnection;
import util.ColAnsi;
import util.ErrorReturn;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

public class Main
{

    static Logger logger;

    static {
        //try (FileInputStream ins = new FileInputStream("../src/log_config")) {
        try (FileInputStream ins = new FileInputStream("./log_config")) {
            LogManager.getLogManager().readConfiguration(ins);
            logger = Logger.getLogger(Main.class.getName());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws IOException
    {
        ColAnsi.init();
        logger.log(Level.INFO, "Запуск сервера");

        PQofSpacemarines pqs = new PQofSpacemarines();
        StudsDbConnection db = new StudsDbConnection();

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            try {
                Thread.sleep(200);
                System.out.println(ColAnsi.ANSI_GREEN + "Пока - пока ☺" + ColAnsi.ANSI_RESET);

                logger.log(Level.INFO, "Отключение сервера");

            } catch (InterruptedException e) {
                logger.log(Level.SEVERE, ColAnsi.ANSI_RED + e.getMessage() + ColAnsi.ANSI_RESET);
            }
        }));

        try {
            db.connectToStuds();
            logger.log(Level.INFO, "\"" + db.getLogin() + "\" подключился к бд");
        } catch (ClassNotFoundException e) {
            logger.log(Level.WARNING, ColAnsi.ANSI_RED + "Не удаётся подключиться к базе данных" + ColAnsi.ANSI_RESET);
            return;
        }

        {
            ErrorReturn error = pqs.addFromDB(db);
            if (error.getCode() != 0) {
                logger.log(Level.WARNING, ColAnsi.ANSI_RED + error.getStatus() + ColAnsi.ANSI_RESET);
                return;
            } else
                logger.log(Level.INFO, ColAnsi.ANSI_GREEN + "Коллекция успешно добавлена из файла" + ColAnsi.ANSI_RESET);
        }

        NetworkConnection connection;

        if (args.length != 0) {
            logger.log(Level.INFO, "Задан порт " + args[0]);
            try {
                connection = new NetworkConnection(Integer.parseInt(args[0]));
            } catch (IllegalStateException | IllegalArgumentException e) {
                logger.log(Level.SEVERE, ColAnsi.ANSI_RED + e.getMessage() + ColAnsi.ANSI_RESET);
                return;
            }
        } else
            connection = new NetworkConnection();

        logger.log(Level.INFO, "Подключение сервера к порту " + connection.getPORT());

        Invoker invoker = new Invoker();
        registerCommands(invoker, pqs, db);

        ExecutorService threadPool = Executors.newCachedThreadPool();

        while (true) {
            ClientConnection clientConnection = connection.getNewClientConnection();

            if (clientConnection == null)
                continue;

            (new InputThread(
                    clientConnection,
                    logger,
                    invoker,
                    threadPool,
                    user -> {
                        try {
                            return db.checkUser(user) == CheckUserResult.RIGHT;
                        } catch (SQLException e) {
                            return false;
                        }
                    }
            )).start();

        }
    }

    private static int scanTerminal() throws IOException
    {
        if (System.in.available() != 0) {
            Scanner scanner = new Scanner(System.in);
            String commandFromConsole;

            try {
                commandFromConsole = scanner.nextLine();
                logger.log(Level.INFO, "Введена команда " + commandFromConsole);
            } catch (NoSuchElementException | IllegalStateException e) {
                logger.log(Level.SEVERE, ColAnsi.ANSI_RED + "Произошло закрытие ввода" + ColAnsi.ANSI_RESET);
                return -1;
            }

            return commandFromConsole.equals("save") ? 0 : commandFromConsole.equals("exit") ? 1 : -1;

        }

        return -1;
    }

    private static void registerCommands(Invoker invoker, PQofSpacemarines pqs, StudsDbConnection db)
    {
        invoker.register("help", new CommandHelp());
        invoker.register("info", new CommandInfo(pqs));
        invoker.register("show", new CommandShow(pqs));
        invoker.register("add", new CommandAdd(db, pqs));
        invoker.register("update", new CommandUpdate(db, pqs));
        invoker.register("remove_by_id", new CommandRemoveById(db, pqs));
        invoker.register("clear", new CommandClear(db, pqs));
        invoker.register("remove_first", new CommandRemoveFirst(db, pqs));
        invoker.register("remove_head", new CommandRemoveHead(db, pqs));
        invoker.register("add_if_min", new CommandAddIfMin(db, pqs));
        invoker.register("filter_contains_name", new CommandFilterContainsName(pqs));
        invoker.register("filter_starts_with_name", new CommandFilterStartsWithName(pqs));
        invoker.register("print_descending", new CommandPrintDescending(pqs));
        invoker.register("register", new CommandRegister(db));
        invoker.register("check_user", new CommandCheckUser(db));
    }
}
