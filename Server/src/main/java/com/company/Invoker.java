package com.company;

import network.PacketCtoS;
import network.PacketStoC;

import java.util.HashMap;

/**
 * Класс, управляющий выполнением комманд
 */
public class Invoker
{
    /**
     * Словарь из комманд -- <имя комманды, <объект комманды, <количество аргументов <i>общее(может быть больше -- тогда в команду нужно передавать список команд)</i>, <количество аргументов <i>для консоли</i>>>>>
     */
    private final HashMap<String, Command> commandMap = new HashMap<>();

    /**
     * Добавить команду <b>без аргументов</b>
     *
     * @param commandName Название команды
     * @param command     Объект комманды
     */
    public void register(String commandName, Command command)
    {
        commandMap.put(commandName, command);
    }

    public PacketStoC execute(PacketCtoS inputPacket)
    {
        String commandName = inputPacket.getCommandName();

        return commandMap.get(commandName).execute(inputPacket);

    }

    /**
     * Проверить, добавлена ли указанная команда
     *
     * @param commandName Название команды
     */
    public boolean isInInvoker(String commandName)
    {
        return !(commandMap.get(commandName) == null);
    }

}
