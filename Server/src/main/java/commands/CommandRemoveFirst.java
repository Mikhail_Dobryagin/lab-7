package commands;

import com.company.Command;
import com.company.PQofSpacemarines;
import database.StudsDbConnection;
import database.User;
import network.PacketCtoS;
import network.PacketStoC;
import spacemarine.SpaceMarine;
import util.ColAnsi;

import java.sql.SQLException;

public class CommandRemoveFirst implements Command
{
    private final PQofSpacemarines pqs;
    private final StudsDbConnection db;

    public CommandRemoveFirst(StudsDbConnection db, PQofSpacemarines pqs)
    {
        this.pqs = pqs;
        this.db = db;
    }

    @Override
    public PacketStoC execute(PacketCtoS inputPacket)
    {
        PacketStoC outputPacket = new PacketStoC();

        User user = inputPacket.getUser();

        pqs.lock.writeLock().lock();
        try {

            SpaceMarine head = pqs.getHead();

            if (head == null) {
                outputPacket.setString("Очередь пуста");
                return outputPacket;
            }

            try {
                if (db.removeSmById(user, head.getId()))
                    outputPacket.setString(pqs.remove_first() ? "Элемент успешно удалён" : "Очередь пуста");
                else
                    outputPacket.setString("Не удалось удалить элемент");
            } catch (SQLException e) {
                outputPacket.setString(ColAnsi.ANSI_RED + e.getMessage() + ColAnsi.ANSI_RESET);
            }
        } finally {
            pqs.lock.writeLock().unlock();
        }

        return outputPacket;
    }
}
