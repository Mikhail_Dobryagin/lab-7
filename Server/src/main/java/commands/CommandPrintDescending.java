package commands;

import com.company.Command;
import com.company.PQofSpacemarines;
import network.PacketCtoS;
import network.PacketStoC;

public class CommandPrintDescending implements Command
{
    private final PQofSpacemarines pqs;

    public CommandPrintDescending(PQofSpacemarines pqs)
    {
        this.pqs = pqs;
    }

    @Override
    public PacketStoC execute(PacketCtoS inputPacket)
    {
        pqs.lock.readLock().lock();

        PacketStoC outputPacket = new PacketStoC();

        try {
            outputPacket.setSpaceMarines(pqs.print_descending());
        } finally {
            pqs.lock.readLock().unlock();
        }

        return outputPacket;
    }
}
