package commands;

import com.company.Command;
import com.company.PQofSpacemarines;
import network.PacketCtoS;
import network.PacketStoC;

public class CommandInfo implements Command
{
    private final PQofSpacemarines pqs;

    public CommandInfo(PQofSpacemarines pqs)
    {
        this.pqs = pqs;
    }

    @Override
    public PacketStoC execute(PacketCtoS inputPacket)
    {
        PacketStoC outputPacket = new PacketStoC();

        pqs.lock.readLock().lock();

        try {
            outputPacket.setString(pqs.info().toString());
        } finally {
            pqs.lock.readLock().unlock();
        }

        return outputPacket;
    }

}
