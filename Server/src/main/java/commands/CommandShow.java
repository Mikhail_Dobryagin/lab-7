package commands;

import com.company.Command;
import com.company.PQofSpacemarines;
import network.PacketCtoS;
import network.PacketStoC;

public class CommandShow implements Command
{
    private final PQofSpacemarines pqs;

    public CommandShow(PQofSpacemarines pqs)
    {
        this.pqs = pqs;
    }


    @Override
    public PacketStoC execute(PacketCtoS inputPacket)
    {
        pqs.lock.readLock().lock();

        PacketStoC outputPacket = new PacketStoC();

        try {
            outputPacket.setSpaceMarines(pqs.show());
            return outputPacket;
        } finally {
            pqs.lock.readLock().unlock();
        }
    }

}
