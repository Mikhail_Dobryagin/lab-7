package commands;

import com.company.Command;
import com.company.PQofSpacemarines;
import database.StudsDbConnection;
import database.User;
import network.PacketCtoS;
import network.PacketStoC;
import util.ColAnsi;

import java.sql.SQLException;

public class CommandClear implements Command
{
    private final PQofSpacemarines pqs;
    private final StudsDbConnection db;

    public CommandClear(StudsDbConnection db, PQofSpacemarines pqs)
    {
        this.pqs = pqs;
        this.db = db;
    }


    @Override
    public PacketStoC execute(PacketCtoS inputPacket)
    {
        PacketStoC outputPacket = new PacketStoC();

        User user = inputPacket.getUser();

        pqs.lock.writeLock().lock();
        try {
            if (db.clearDB(user)) {
                pqs.setPq(db.getSpaceMarines());
                outputPacket.setString("Коллекция успешно очищена");
            } else
                outputPacket.setString("Пользователь не создавал ни одного объекта");
        } catch (SQLException e) {
            outputPacket.setString(ColAnsi.ANSI_RED + e.getMessage() + ColAnsi.ANSI_RESET);
            return outputPacket;
        } finally {
            pqs.lock.writeLock().unlock();
        }


        return outputPacket;
    }

}
