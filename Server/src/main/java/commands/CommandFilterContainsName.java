package commands;

import com.company.Command;
import com.company.PQofSpacemarines;
import network.PacketCtoS;
import network.PacketStoC;

public class CommandFilterContainsName implements Command
{
    private final PQofSpacemarines pqs;

    public CommandFilterContainsName(PQofSpacemarines pqs)
    {
        this.pqs = pqs;
    }

    @Override
    public PacketStoC execute(PacketCtoS inputPacket)
    {
        pqs.lock.readLock().lock();

        PacketStoC outputPacket = new PacketStoC();

        try {
            outputPacket.setSpaceMarines(pqs.filter_contains_name(inputPacket.getStringArg()));
        } finally {
            pqs.lock.readLock().unlock();
        }

        return outputPacket;
    }
}
