package commands;

import com.company.Command;
import com.company.PQofSpacemarines;
import network.PacketCtoS;
import network.PacketStoC;

public class CommandHelp implements Command
{

    @Override
    public PacketStoC execute(PacketCtoS inputPacket)
    {
        PacketStoC outputPacket = new PacketStoC();

        outputPacket.setString(PQofSpacemarines.help().toString());
        return outputPacket;
    }
}
