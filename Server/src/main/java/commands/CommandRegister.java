package commands;

import com.company.Command;
import database.CheckUserResult;
import database.StudsDbConnection;
import database.User;
import network.PacketCtoS;
import network.PacketStoC;

import java.sql.SQLException;

public class CommandRegister implements Command
{
    StudsDbConnection db;

    public CommandRegister(StudsDbConnection db)
    {
        this.db = db;
    }

    @Override
    public PacketStoC execute(PacketCtoS inputPacket)
    {
        PacketStoC outputPacket = new PacketStoC();

        User user = inputPacket.getUser();

        try {
            if (db.checkUser(user) != CheckUserResult.WRONG_USER) {
                outputPacket.setString("Пользователь с таким логином уже существует");
                return outputPacket;
            }

            db.registerUser(user);
            outputPacket.setString("Вы успешно зарегистрированы");
            return outputPacket;

        } catch (SQLException e) {
            outputPacket.setString(e.getMessage());
            return outputPacket;
        }
    }
}
