package commands.server;

import com.company.CommandProcessingServerReturn;
import network.PacketStoC;


public class CommandSout implements CommandProcessingServerReturn
{
    @Override
    public void execute(PacketStoC answerPacket)
    {
        if (answerPacket.getString() != null)
            System.out.println(answerPacket.getString());
    }
}
