package commands.server;

import com.company.CommandProcessingServerReturn;
import network.PacketStoC;
import util.ErrorReturn;


public class CommandShow implements CommandProcessingServerReturn
{

    public static void longLine()
    {
        System.out.println("________________________________________________________________________________________________________________________");
    }

    @Override
    public void execute(PacketStoC answerPacket)
    {
        ErrorReturn errorReturn = answerPacket.getErrorReturn();

        if (errorReturn.getCode() != 0) {
            System.out.println(errorReturn.getStatus());
            return;
        }

        System.out.println();
        longLine();

        while (!answerPacket.getSpaceMarines().isEmpty())
            System.out.println(answerPacket.getSpaceMarines().poll().show());

        longLine();
        System.out.println();
    }
}
