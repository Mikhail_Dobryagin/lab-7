package commands.client;

import com.company.Command;
import network.PacketCtoS;
import util.ErrorReturn;

import java.util.LinkedList;

public class CommandFilterStartsWithName implements Command
{
    @Override
    public ErrorReturn execute(PacketCtoS outputPacket)
    {
        return null;
    }

    @Override
    public ErrorReturn execute(Object[] args, LinkedList<String> listWithArgs, PacketCtoS outputPacket)
    {
        outputPacket.setStringArg((String) args[0]);
        return ErrorReturn.OK();
    }
}
