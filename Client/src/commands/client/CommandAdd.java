package commands.client;

import com.company.Command;
import network.PacketCtoS;
import spacemarine.Chapter;
import spacemarine.SpaceMarine;
import util.EndOfScanException;
import util.ErrorReturn;

import java.util.LinkedList;


public class CommandAdd implements Command
{
    @Override
    public ErrorReturn execute(Object[] args, LinkedList<String> list, PacketCtoS outputPacket)
    {
        String[] stringArgs = new String[SpaceMarine.countOfArgumentsWithoutId + 1];
        stringArgs[0] = "1";

        for (int i = 1; i <= args.length; i++)
            stringArgs[i] = (String) args[i - 1];

        if (args.length == SpaceMarine.countOfArgumentsWithoutId - 2 && args[args.length - 1].equals("+")) {
            if (list.size() <= 1)
                return new ErrorReturn(2, "Мало аргументов для вызова функции");

            stringArgs[stringArgs.length - 2] = list.getFirst();
            list.removeFirst();
            stringArgs[stringArgs.length - 1] = list.getFirst();
            list.removeFirst();

            if (stringArgs[stringArgs.length - 2].equals("&&&") || stringArgs[stringArgs.length - 1].equals("&&&"))
                return new ErrorReturn(2, "Неверно введены аргументы");
        }

        try {
            outputPacket.setSpaceMarine(SpaceMarine.newSpaceMarine(stringArgs));
            return ErrorReturn.OK();
        } catch (IllegalArgumentException | Chapter.MakeChapterException | SpaceMarine.MakeSpacemarineException e) {
            return new ErrorReturn(2, e.getMessage());
        }
    }

    @Override
    public ErrorReturn execute(PacketCtoS outputPacket)
    {
        String[] args = new String[SpaceMarine.countOfArgumentsWithoutId + 1]; //...+1(id)
        args[0] = "1";

        {
            String[] spaceMarineWithoutId;
            try {
                spaceMarineWithoutId = SpaceMarine.scanSpaceMarine();
            } catch (EndOfScanException e) {
                return ErrorReturn.endOfScan();
            } catch (IllegalArgumentException | SpaceMarine.MakeSpacemarineException | Chapter.MakeChapterException e) {
                return new ErrorReturn(2, e.getMessage());
            }

            System.arraycopy(spaceMarineWithoutId, 0, args, 1, SpaceMarine.countOfArgumentsWithoutId);
        }

        try {
            outputPacket.setSpaceMarine(SpaceMarine.newSpaceMarine(args));
            return ErrorReturn.OK();
        } catch (IllegalArgumentException | Chapter.MakeChapterException | SpaceMarine.MakeSpacemarineException e) {
            return new ErrorReturn(2, e.getMessage());
        }

    }
}

