package commands.client;

import com.company.Command;
import network.PacketCtoS;
import util.ColAnsi;
import util.ErrorReturn;

import java.util.LinkedList;

public class CommandExit implements Command
{
    @Override
    public ErrorReturn execute(Object[] args, LinkedList<String> listWithArgs, PacketCtoS outputPacket)
    {
        return null;
    }

    @Override
    public ErrorReturn execute(PacketCtoS outputPacket)
    {
        System.out.println(ColAnsi.ANSI_GREEN + "Пока - пока ☺" + ColAnsi.ANSI_RESET);
        System.exit(0);
        return ErrorReturn.OK();
    }

}
