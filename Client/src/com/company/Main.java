package com.company;

import commands.server.CommandSout;
import database.CheckUserResult;
import database.User;
import network.Connection;
import network.PacketCtoS;
import network.PacketStoC;
import util.ColAnsi;
import util.ErrorReturn;

import java.io.Console;
import java.io.IOException;
import java.nio.channels.NonReadableChannelException;
import java.nio.channels.NonWritableChannelException;
import java.nio.channels.NotYetConnectedException;
import java.util.*;

public class Main
{
    static User user;

    public static void main(String[] args) throws IOException, ClassNotFoundException
    {

        ColAnsi.init();

        LinkedList<String> list = new LinkedList<>();

        Invoker invoker = new Invoker();

        registerCommands(invoker, list);

        HashSet<String> commandsSet = new HashSet<>();

        Connection connection = null;

        try {
            if (args.length >= 2)
                connection = new Connection(args[0], Integer.parseInt(args[1]));
            if (args.length == 1)
                connection = new Connection(Integer.parseInt(args[0]));
        } catch (IllegalArgumentException | IllegalStateException e) {
            System.out.println(ColAnsi.ANSI_RED + e.getMessage() + ColAnsi.ANSI_RESET);
            return;
        }

        if (args.length == 0)
            connection = new Connection();

        user = new User();

        //Авторизация
        do {
            System.out.print("Хотите ли вы авторизоваться/зарегистрироваться (si/su): ");
            Console console = System.console();

            String sign = console.readLine();

            if (sign == null)
                continue;

            if (!(sign.equals("si") || sign.equals("su"))) {
                System.out.println(ColAnsi.ANSI_YELLOW + "Введите \"si\" или \"su\"" + ColAnsi.ANSI_RESET);
                user = new User();
                continue;
            }
            if (sign.equals("si")) {
                logIn(connection, console);
            } else {
                registerUser(connection, console);
            }

        } while (user.getLogin() == null);

        while (true) {
            if (list.isEmpty())           //Если команда вводится с консоли
            {
                commandsSet.clear();
                String[] commandWithArgs;

                try {
                    commandWithArgs = scanCommand();
                } catch (NoSuchElementException | IllegalStateException e) {
                    System.out.println(ColAnsi.ANSI_RED + ErrorReturn.endOfScan().getStatus() + ColAnsi.ANSI_RESET);
                    return;
                }

                String commandName = commandWithArgs[0];

                if (commandWithArgs.length == 1) {
                    PacketCtoS outputPacket = new PacketCtoS();


                    ErrorReturn error = invoker.execute(commandName, outputPacket);

                    if (error.getCode() == 1) {
                        System.out.println(ColAnsi.ANSI_RED + error.getStatus() + ColAnsi.ANSI_RESET);
                        return;
                    } else if (error.getCode() == 2) {
                        if (error.getStatus() != null)
                            System.out.println(ColAnsi.ANSI_YELLOW + error.getStatus() + ColAnsi.ANSI_RESET);
                        continue;
                    }

                    try {
                        connection.setChannel();
                        connection.send(outputPacket);
                    } catch (IOException | NotYetConnectedException | NonWritableChannelException e) {
                        System.out.println(ColAnsi.ANSI_YELLOW + "Невозможно отправить данные на сервер " + ColAnsi.ANSI_RESET);
                        continue;
                    }

                    if (invoker.hasAnswer(commandName))
                        invoker.processAnswerFromServer(commandName, connection.getData());

                    connection.closeChannel();
                } else {
                    if (invoker.getCountOfConsoleArgs(commandName) != commandWithArgs.length - 1) {
                        System.out.println(ColAnsi.ANSI_YELLOW + "Аргументы введены неправильно" + ColAnsi.ANSI_RESET);
                        continue;
                    }

                    PacketCtoS outputPacket = new PacketCtoS();


                    ErrorReturn error = invoker.execute(commandName, new Object[]{commandWithArgs[1]}, list, outputPacket);

                    if (error.getCode() == 1) {

                        if (error.getStatus() != null) {
                            System.out.println();
                            System.out.println(ColAnsi.ANSI_RED + error.getStatus() + ColAnsi.ANSI_RESET);
                        }
                        return;
                    }

                    if (error.getCode() == 0) {
                        actionWithoutError(invoker, commandName, outputPacket, error, connection);
                        if (error.getStatus() != null)
                            System.out.println(error.getStatus());
                    } else if (error.getStatus() != null)
                        System.out.println(ColAnsi.ANSI_YELLOW + error.getStatus() + ColAnsi.ANSI_RESET);

                }

                continue;
            }

            if (list.getFirst().equals("&&&")) {
                list.removeFirst();
                continue;
            }

            if (list.getFirst().contains("&&&")) {
                System.out.println(ColAnsi.ANSI_RED + "Неверно введены аргументы" + ColAnsi.ANSI_RESET);
                list.clear();
                continue;
            }

            String[] commandNameWithArg = list.getFirst().split(" ", 2);
            list.removeFirst();

            String commandName = commandNameWithArg[0];

            if (!list.isEmpty() && commandName.equals("execute_script")) {
                if (commandsSet.contains(list.getFirst())) {
                    System.out.println(ColAnsi.ANSI_RED + "У вас нашли рекурсию\nЗдоровья погибшим" + ColAnsi.ANSI_RESET);
                    list.clear();
                    continue;
                }
                commandsSet.add(list.getFirst());
            }


            int countOfArgs = invoker.getCountOfArgs(commandName);
            int countOfConsoleArgs = invoker.getCountOfConsoleArgs(commandName);

            if (countOfArgs < 0) {
                System.out.println(ColAnsi.ANSI_RED + "Команды " + commandName + " не существует" + ColAnsi.ANSI_RESET);
                list.clear();
                continue;
            }

            if (countOfArgs - countOfConsoleArgs > list.size()) {
                System.out.println(ColAnsi.ANSI_RED + "Мало аргументов для вызова функции" + ColAnsi.ANSI_RESET);
                list.clear();
                continue;
            }

            ErrorReturn error = null;

            Object[] argsForCommand = new Object[countOfArgs];
            for (int i = 0; i < countOfArgs; i++) {
                if (i == 0 && commandNameWithArg.length == 2)
                    argsForCommand[i] = commandNameWithArg[1];
                else {
                    argsForCommand[i] = list.getFirst();
                    list.removeFirst();
                }

                if (((String) argsForCommand[i]).contains("&&&")) {
                    error = new ErrorReturn(2, "Неверно введены аргументы" + "\n" + argsForCommand[i]);
                    break;
                }
            }

            if (error != null) {
                System.out.println(ColAnsi.ANSI_RED + error.getStatus() + ColAnsi.ANSI_RESET);
                list.clear();
                continue;
            }

            PacketCtoS outputPacket = new PacketCtoS();


            while (user.getLogin() == null)
                logIn(connection, System.console());

            error = countOfArgs == 0 ? invoker.execute(commandName, outputPacket) : invoker.execute(commandName, argsForCommand, list, outputPacket);

            if (error.getCode() == 1) {
                System.out.println();
                System.out.println(ColAnsi.ANSI_RED + error.getStatus() + ColAnsi.ANSI_RESET);
                return;
            }

            if (error.getCode() == 0) {
                actionWithoutError(invoker, commandName, outputPacket, error, connection);

                continue;
            }

            System.out.println(ColAnsi.ANSI_YELLOW + error.getStatus() + ColAnsi.ANSI_RESET);
            list.clear();

        }

    }

    private static void actionWithoutError(Invoker invoker, String commandName, PacketCtoS outputPacket, ErrorReturn error, Connection connection) throws IOException, ClassNotFoundException
    {

        if (error.getStatus() != null)
            System.out.println(ColAnsi.ANSI_GREEN + error.getStatus() + ColAnsi.ANSI_RESET);

        if (!commandName.equals("execute_script")) {
            try {
                connection.setChannel();
            } catch (IOException e) {
                System.out.println(ColAnsi.ANSI_RED + "Невозможно присоединиться к серверу" + ColAnsi.ANSI_RESET);
                return;
            }

            try {
                connection.send(outputPacket);
            } catch (IOException e) {
                System.out.println(ColAnsi.ANSI_RED + "Произошла ошибка вывода" + ColAnsi.ANSI_RESET);
                return;
            } catch (NotYetConnectedException e) {
                System.out.println(ColAnsi.ANSI_RED + "Канал не присоединён к серверу" + ColAnsi.ANSI_RESET);
                return;
            } catch (NonWritableChannelException e) {
                System.out.println(ColAnsi.ANSI_RED + "Невозможно осуществить отправку данных на сервер" + ColAnsi.ANSI_RESET);
                return;
            }

            try {
                if (invoker.hasAnswer(commandName))
                    invoker.processAnswerFromServer(commandName, connection.getData());
            } catch (IOException e) {
                System.out.println(ColAnsi.ANSI_RED + "Произошла ошибка при чтении данных с сервера" + ColAnsi.ANSI_RESET);
                return;
            } catch (NonReadableChannelException e) {
                System.out.println(ColAnsi.ANSI_RED + "Невозможно получить данные с сервера" + ColAnsi.ANSI_RESET);
                return;
            } catch (NotYetConnectedException e) {
                System.out.println(ColAnsi.ANSI_RED + "Канал не присоединён к серверу" + ColAnsi.ANSI_RESET);
                return;
            }

            connection.closeChannel();
        }
    }


    public static String[] scanCommand()
    {
        String[] lines;
        Scanner in = new Scanner(System.in);

        lines = in.nextLine().split(" ", 2);
        return lines;
    }

    private static void registerCommands(Invoker invoker, LinkedList<String> list)
    {
        invoker.register("help", new commands.client.CommandHelp());
        invoker.register("info", new commands.client.CommandInfo());
        invoker.register("show", new commands.client.CommandShow());
        invoker.register("add", new commands.client.CommandAdd(), 8, true);
        invoker.register("add_if_min", new commands.client.CommandAdd(), 8, true);
        invoker.register("update", new commands.client.CommandUpdate(), 9, 1, true);
        invoker.register("remove_by_id", new commands.client.CommandRemoveById(), 1, 1, true);
        invoker.register("clear", new commands.client.CommandClear());
        invoker.register("execute_script", new commands.client.CommandExecuteScript(list), 1, 1);
        invoker.register("exit", new commands.client.CommandExit());
        invoker.register("remove_first", new commands.client.CommandRemoveFirst());
        invoker.register("remove_head", new commands.client.CommandRemoveHead());
        invoker.register("filter_contains_name", new commands.client.CommandFilterContainsName(), 1, 1, true);
        invoker.register("filter_starts_with_name", new commands.client.CommandFilterStartsWithName(), 1, 1, true);
        invoker.register("print_descending", new commands.client.CommandPrintDescending());

        invoker.registerSecondCommand("help", new CommandSout());
        invoker.registerSecondCommand("info", new CommandSout());
        invoker.registerSecondCommand("show", new commands.server.CommandShow());
        invoker.registerSecondCommand("add", new commands.server.CommandSout());
        invoker.registerSecondCommand("add_if_min", new commands.server.CommandSout());
        invoker.registerSecondCommand("update", new commands.server.CommandSout());
        invoker.registerSecondCommand("remove_by_id", new commands.server.CommandSout());
        invoker.registerSecondCommand("clear", new commands.server.CommandSout());
        invoker.registerSecondCommand("remove_first", new commands.server.CommandSout());
        invoker.registerSecondCommand("remove_head", new commands.server.CommandShow());
        invoker.registerSecondCommand("filter_contains_name", new commands.server.CommandShow());
        invoker.registerSecondCommand("filter_starts_with_name", new commands.server.CommandShow());
        invoker.registerSecondCommand("print_descending", new commands.server.CommandShow());
    }

    private static boolean logIn(Connection connection, Console console)
    {
        user = User.scanUser(console);

        if (user.getLogin() == null)
            return false;

        user.encodePassword();

        try {
            connection.setChannel();
            PacketCtoS netArgs = new PacketCtoS("check_user", user);

            connection.send(netArgs);

            PacketStoC answer = connection.getData();

            ErrorReturn errorReturn = answer.getErrorReturn();

            if (errorReturn.getCode() != 0) {
                System.out.println(errorReturn.getStatus());
                user = new User();
                return false;
            }

            CheckUserResult result = answer.getResult();


            connection.closeChannel();

            if (result == CheckUserResult.RIGHT) {
                System.out.println(ColAnsi.ANSI_GREEN + "Вы успешно авторизованы" + ColAnsi.ANSI_RESET);
                return true;
            }

            System.out.println(ColAnsi.ANSI_YELLOW + (result == CheckUserResult.WRONG_PASSWORD ? "Неверный пароль" : "Пользователя с таким логином не существует") + ColAnsi.ANSI_RESET);
            user = new User();

            //Регистрация
            do {
                System.out.print("Хотите ли вы зарегистрироваться (+/-): ");
                String sign = console.readLine();

                if (sign == null || sign.equals("-"))
                    return false;
                if (!sign.equals("+")) {
                    System.out.println(ColAnsi.ANSI_YELLOW + "Введите + или -" + ColAnsi.ANSI_RESET);
                    continue;
                }

                if (registerUser(connection, console))
                    return true;

                user = new User();
                return false;


            } while (true);

        } catch (IOException | ClassNotFoundException e) {
            System.out.println(ColAnsi.ANSI_RED + e.getMessage() + ColAnsi.ANSI_RESET);
            user = new User();
            return false;
        }
    }

    private static boolean registerUser(Connection connection, Console console)
    {
        do {
            user = User.scanUser(console);

            if (user.getLogin() == null)
                return false;

            System.out.print("Повторите пароль: ");

            String subPassword;
            try {
                subPassword = new String(console.readPassword());
            } catch (NullPointerException e) {
                subPassword = " ";
            }

            if (Arrays.equals(user.getPassword(), subPassword.getBytes()))
                break;

            System.out.println(ColAnsi.ANSI_YELLOW + "Пароли не совпадают" + ColAnsi.ANSI_RESET);
        } while (true);

        user.encodePassword();

        try {
            PacketCtoS netArgs = new PacketCtoS("check_user", user);
            connection.setChannel();
            connection.send(netArgs);

            PacketStoC answer = connection.getData();

            ErrorReturn errorReturn = answer.getErrorReturn();

            if (errorReturn.getCode() > 0) {
                System.out.println(errorReturn.getStatus());
                user = new User();
                return false;
            }

            CheckUserResult result = answer.getResult();

            if (result != CheckUserResult.WRONG_USER) {
                System.out.println(ColAnsi.ANSI_YELLOW + "Пользователь с таким логином уже занят" + ColAnsi.ANSI_RESET);
                user = new User();
                return false;
            }

            connection.closeChannel();

            connection.setChannel();
            netArgs = new PacketCtoS("register", user);
            connection.send(netArgs);

            System.out.println(connection.getData().getString());
            connection.closeChannel();
            return true;

        } catch (IOException | ClassNotFoundException e) {
            user = new User();
            System.out.println(ColAnsi.ANSI_RED + e.getMessage() + ColAnsi.ANSI_RESET);
            return false;
        }

    }

}
