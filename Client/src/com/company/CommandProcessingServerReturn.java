package com.company;

import network.PacketStoC;


public interface CommandProcessingServerReturn
{
    void execute(PacketStoC argsFromServer);
}
