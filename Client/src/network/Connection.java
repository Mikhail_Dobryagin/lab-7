package network;

import java.io.*;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

public class Connection
{
    private SocketAddress socketAddress;
    private SocketChannel channel = null;

    ////////////////////////////////////////////////////////////////////

    public SocketAddress getSocketAddress()
    {
        return socketAddress;
    }

    public void setSocketAddress(SocketAddress socketAddress)
    {
        this.socketAddress = socketAddress;
    }

    public void setChannel() throws IOException
    {
        long time1 = System.currentTimeMillis();
        long time2 = time1;

        while (time2 - time1 <= 5000) {
            try {
                channel = SocketChannel.open(socketAddress);
            } catch (IOException e) {
                time2 = System.currentTimeMillis();
                continue;
            }

            break;
        }


        if (channel == null || !channel.isOpen())
            channel = SocketChannel.open(socketAddress);

        channel.configureBlocking(false);
    }

    public void setChannel(SocketChannel channel)
    {
        this.channel = channel;
    }

    public SocketChannel getChannel()
    {
        return channel;
    }
    ////////////////////////////////////////////////////////////////////

    public Connection(String IP, int PORT)
    {
        socketAddress = new InetSocketAddress(IP, PORT);
    }

    public Connection(int PORT)
    {
        socketAddress = new InetSocketAddress("localhost", PORT);
    }

    public Connection()
    {
        socketAddress = new InetSocketAddress("localhost", 7531);
    }


    public PacketStoC getData() throws IOException, ClassNotFoundException
    {
        ByteBuffer buffer = ByteBuffer.allocate(100000);

        int countOfBytes;

        while (buffer.position() < 4 + 2 + 2) //+2? (May be, header)
            channel.read(buffer);


        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(buffer.array());
        ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);

        countOfBytes = objectInputStream.readInt();

        while (buffer.position() != countOfBytes)
            channel.read(buffer);

        byteArrayInputStream = new ByteArrayInputStream(buffer.array());
        objectInputStream = new ObjectInputStream(byteArrayInputStream);
        objectInputStream.readInt();
        return (PacketStoC) objectInputStream.readObject();
    }

    public void send(PacketCtoS outputArgs) throws IOException
    {
        if (channel == null)
            setChannel();

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
        objectOutputStream.writeObject(outputArgs);

        byte[] outputBytes = byteArrayOutputStream.toByteArray();
        ByteBuffer buffer = ByteBuffer.wrap(outputBytes);

        while (buffer.hasRemaining())
            channel.write(buffer);
    }

    public void closeChannel() throws IOException
    {
        channel.close();
    }
}
