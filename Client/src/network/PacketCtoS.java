package network;

import database.User;
import spacemarine.SpaceMarine;

import java.io.Serializable;

public class PacketCtoS implements Serializable
{
    public static final long serialVersionUID = 11L;
    private String commandName = null;
    private User user = null;
    private SpaceMarine spaceMarine = null;
    private Long id = null;
    private String stringArg = null;

    public PacketCtoS()
    {
    }

    public PacketCtoS(String commandName, User user)
    {
        this.commandName = commandName;
        this.user = user;
    }

    public PacketCtoS(String commandName, User user, SpaceMarine spaceMarine)
    {
        this(commandName, user);
        this.spaceMarine = spaceMarine;
    }

    public String getCommandName()
    {
        return commandName;
    }

    public User getUser()
    {
        return user;
    }

    public SpaceMarine getSpaceMarine()
    {
        return spaceMarine;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public String getStringArg()
    {
        return stringArg;
    }

    public void setStringArg(String stringArg)
    {
        this.stringArg = stringArg;
    }

    public void setCommandName(String commandName)
    {
        this.commandName = commandName;
    }

    public void setUser(User user)
    {
        this.user = user;
    }

    public void setSpaceMarine(SpaceMarine spaceMarine)
    {
        this.spaceMarine = spaceMarine;
    }

    public boolean hasSpaceMarine()
    {
        return spaceMarine != null;
    }
}
